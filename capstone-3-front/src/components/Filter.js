import {Col, Button, Form} from 'react-bootstrap';

export default function Filters(){


	return (
		<Col>
		<div className="filters mt-5">
			<span className="mt-4 ms-4 me-4 mb-2">Filters</span>
			<span className="mt-2 ms-4 me-4">
				<Form.Check
					inline
					label="Low to high"
					name="group1"
					type="radio"
					id={`inline-1`}
				/>
			</span>

			<span className="mt-2 ms-4 me-4">
			<Form.Check
				inline
				label="High to low"
				name="group1"
				type="radio"
				id={`inline-2`}
			/>
			</span>	

			<button className="m-4" variant="light">Clear filters</button>

		</div>
	</Col>
	
	);
};

